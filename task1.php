<?php
$growths = [ 1 => 150,
            2 => 160,
            3 => 170,
            4 => 180,
            5 => 190
            ];

$surnames = [1 => "Pupkin",
             2 => "Uipkin",
             3 => "Utkin",
             4 => "Sutkin",
             5 => "Shatkin"
            ];

echo "Рост самого высокого студента {$growths[5]} = {$surnames[5]}";